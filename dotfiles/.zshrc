[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"  # This loads RVM into a shell session.

# Path to your oh-my-zsh configuration.
export ZSH=$HOME/.oh-my-zsh

# Set to the name theme to load.
# Look in ~/.oh-my-zsh/themes/
export ZSH_THEME="trym"

# Set to this to use case-sensitive completion
# export CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
export DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# export DISABLE_LS_COLORS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git github ruby osx brew gem pip rails3 bundler cap)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
#export PATH="$PATH:/Users/trym/.ec2/bin:/Users/trym/.aws_rds/bin"

unsetopt auto_name_dirs

# Disable annoying correction
unsetopt correct_all
setopt correct

export EDITOR="mvim -f"
export CUCUMBER_FORMAT="pretty"

# history search up/down
bindkey "^[[A" history-search-backward
bindkey "^[[B" history-search-forward
 
