#alias cucumber='bundle exec cucumber'
alias cucumber='$GEM_HOME/bin/cucumber'

# git stuff
alias gco='git checkout'
alias g='git'

alias bx='bundle exec'
alias ti="terminitor"
alias m='mvim'
alias m.='mvim .'

alias b="bundle"
alias bi="b install --path vendor"
alias bu="b update"
alias be="b exec"
alias binit="bi && b package && echo 'vendor/ruby' >> .gitignore"
